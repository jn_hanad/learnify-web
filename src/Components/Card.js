import { Avatar, makeStyles } from "@material-ui/core";
import React from "react";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
export default function MyCard(props) {
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },

    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
  }));
  const classes = useStyles();
  const email = `mailto:${props.description}`;
  return (
    <div className="myCard">
      <div className="card-content">
        <Avatar className={classes.large} src={props.image}></Avatar>
        <div className="inner-card">
          <h3>{props.name}</h3>
          <div className="email">
            <div className="mIcon">
              <MailOutlineIcon></MailOutlineIcon>
            </div>
            <a href={email}>{props.description}</a>
          </div>
        </div>
      </div>
    </div>
  );
}
