import "./App.css";
import ButtonAppBar from "./Components/appBar";
import React from "react";
import FadeIn from "react-fade-in";

function How() {
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);

  return (
    <div className="App">
      <ButtonAppBar
        prevOpen={prevOpen}
        handleClose={handleClose}
        handleListKeyDown={handleListKeyDown}
        handleToggle={handleToggle}
        anchorRef={anchorRef}
        open={open}
        setOpen={setOpen}
      ></ButtonAppBar>
      {!open ? (
        <div className="title-sec">
          <FadeIn>
            {" "}
            <div className="mItem">
              <li>Comment jouer?</li>
            </div>
          </FadeIn>
        </div>
      ) : (
        <div></div>
      )}
      <div className="acceuil-content">
        <div className="innerAcceuil">
          <p>
            1.Dans la première page , cliquer sur “ALLONS-Y” et choisir un
            compte google pour sauvegarder la trace et le parcours.
          </p>
          <br />
          <div className="imgW"></div>
          <br />
          <p>
            2.Une fois le compte sélectionné , créer le profil learnify : nom et
            avatar.
          </p>
          <br />
          <div className="imgT"></div>
          <br />
          <p>
            3.choisir un domaine et passer le test de niveau pour débloquer les
            niveaux et commencer l’experience.
          </p>
          <br />
          <div className="imgS"></div>
          <br />
        </div>
      </div>
    </div>
  );
}

export default How;
