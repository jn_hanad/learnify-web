import ButtonAppBar from "./Components/appBar";
import "./App.css";
import React from "react";
import FadeIn from "react-fade-in";
import apk from "./assets/app-release.apk";
import GetAppIcon from "@material-ui/icons/GetApp";
function Installation() {
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);

  return (
    <div className="App">
      <ButtonAppBar
        prevOpen={prevOpen}
        handleClose={handleClose}
        handleListKeyDown={handleListKeyDown}
        handleToggle={handleToggle}
        anchorRef={anchorRef}
        open={open}
        setOpen={setOpen}
      ></ButtonAppBar>
      {!open ? (
        <div className="title-sec">
          <FadeIn>
            {" "}
            <div className="mItem">
              <li>Lien et guide d'installation</li>
            </div>
          </FadeIn>
        </div>
      ) : (
        <div></div>
      )}

      <div className="acceuil-content">
        <div className="innerIns">
          <p>
            Finie l’attente! Télécharger l’application , commencer l’aventure !
            Cliquer sur le bouton et suivre les étapes suivantes :
          </p>
          <br />
          <div className="download-button">
            <a href={apk} download>
              Télécharger
            </a>
            <div className="dIcon">
              <GetAppIcon style={{ fontSize: 18 }}></GetAppIcon>
            </div>
          </div>
          <br />

          <p>
            Une fois le fichier .apk téléchargé sur votre appareil , ouvrez le
            fichier et attendez que l’application s’installe. Maintenant ,
            Learnify est présente dans votre accueil. Jouer!
          </p>
        </div>
      </div>
    </div>
  );
}

export default Installation;
