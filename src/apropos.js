import Nada from "./assets/nada.png";
import Meriem from "./assets/meriem.png";
import Rania from "./assets/rania.png";
import Sarra from "./assets/sarra.png";
import Houssem from "./assets/houssem.png";
import Younes from "./assets/younes.png";
import "./App.css";
import ButtonAppBar from "./Components/appBar";
import React from "react";
import FadeIn from "react-fade-in";
import MyCard from "./Components/Card";

const members = [
  {
    name: "Meriem Belkacemi",
    description: "jm_belkacemi@esi.dz",
    image: Meriem,
  },
  {
    name: "Fatmazohra Rezkellah",
    description: "jf_rezkellah@esi.dz",
    image: Rania,
  },
  {
    name: "Sarra Bendaho",
    description: "js_bendaho@esi.dz",
    image: Sarra,
  },
  {
    name: "Nada Hanad",
    description: "jn_hanad@esi.dz",
    image: Nada,
  },
  {
    name: "Houssem Achab",
    description: "jh_achab@esi.dz ",
    image: Houssem,
  },
  {
    name: "Younes Djemmal",
    description: "iy_djemmal@esi.dz ",
    image: Younes,
  },
];
function Apropos() {
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);

  return (
    <div className="App">
      <ButtonAppBar
        prevOpen={prevOpen}
        handleClose={handleClose}
        handleListKeyDown={handleListKeyDown}
        handleToggle={handleToggle}
        anchorRef={anchorRef}
        open={open}
        setOpen={setOpen}
      ></ButtonAppBar>
      {!open ? (
        <div className="title-sec">
          <FadeIn>
            {" "}
            <div className="mItem">
              <li>Contact</li>
            </div>
          </FadeIn>
        </div>
      ) : (
        <div></div>
      )}
      <div className="acceuil-content">
        <div className="innerContact">
          <h4>Equipe réalisatrice :</h4>
          {members.map((e) => {
            return (
              <div>
                <MyCard
                  name={e.name}
                  description={e.description}
                  image={e.image}
                ></MyCard>
                <br />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Apropos;
