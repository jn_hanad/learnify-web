import "./App.css";
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Acceuil from "./acceuil";
import Description from "./description.js";
import installation from "./installation";
import How from "./how";
import Apropos from "./apropos";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Acceuil}></Route>
        <Route path="/acceuil" component={Acceuil} />
        <Route path="/description" component={Description} />
        <Route path="/installation" component={installation} />
        <Route path="/comment-jouer" component={How} />
        <Route path="/contact" component={Apropos} />
        /installation Description
      </Switch>
    </Router>
  );
}

export default App;
